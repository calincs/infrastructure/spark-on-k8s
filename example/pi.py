from __future__ import print_function

import sys
from random import random
from operator import add

from pyspark.sql import SparkSession


if __name__ == "__main__":
    """
        Usage: pi [partitions]
    """
    spark = SparkSession\
        .builder\
        .appName("S3PythonPi")\
        .getOrCreate()

    # Read data from an S3 bucket
    #my_dataframe = spark.read.json("s3a://users/zacanbot/data/")
    my_text = spark.sparkContext.textFile("s3a://spark-hs/sample-data.json")

    partitions = int(sys.argv[1]) if len(sys.argv) > 1 else 2
    n = 100000 * partitions

    def f(_):
        x = random() * 2 - 1
        y = random() * 2 - 1
        return 1 if x ** 2 + y ** 2 <= 1 else 0

    count = spark.sparkContext.parallelize(range(1, n + 1), partitions).map(f).reduce(add)
    answer = "Pi is roughly %f" % (4.0 * count / n)
    print("\n %s \n" % answer)

    # Persist some data in S3 storage
    #my_dataframe.write.format("csv").save("s3a://users/zacanbot/data/")

    spark.stop()