# Spark on K8s

Spark Operator and related resources.

## Overview of the Process

1. When a `SparkApplication` manifest gets deployed, Kubernetes will invoke the Spark Operator to handle it. This manifest describes the job that will be submitted to Spark.
2. The operator downloads the Spark script and dependencies from an S3 bucket specified in the manifest.
3. The operator deploys a Spark Driver as a Pod (or Pods) that will coordinate the task distribution.
4. Spark Executors that run the actual tasks are then created according to the specification in the manifest.
5. The tasks can be monitored while they are running, but the Pods are destroyed after they have finished.
6. The execution is logged against a Spark History server, which allows inspection after the job has completed.
7. Results should be persisted in an S3 bucket by the Spark script.

## Submitting Jobs

Running a Spark job involves a few steps:

* Creating a Python script to run in PySpark
* Uploading the script and dependent resources to an S3 bucket
* Creating a k8s manifest (.yaml file) that specifies the job
* Creating and managing the job with [Sparkctl](Sparkctl.md)

### Creating the PySpark script

The first step is to develop the Python script that will execute in PySpark. This involves setting up the Spark Context, reading some data from S3 storage, running an algorithm, and then writing results back to the S3 bucket.

Look at the simple example [here](example/pi.py) that calculates the value of pi and shows a few example read/write statements to S3.

### Uploading the script and dependencies

For the Spark operator to find the script, it needs to be in storage that is accessible from Kubernetes. We will use S3 storage for that purpose on the LINCS infrastructure. Our S3 storage is located at [https://aux.lincsproject.ca](https://aux.lincsproject.ca) and you can use any of the buckets that you have access to to store the script and any of its dependencies. You can use the website to upload files or any S3-compatible client. Minio's own `mc` works well and is a complete solution for interacting with your bucket: [https://docs.min.io/docs/minio-client-complete-guide.html](https://docs.min.io/docs/minio-client-complete-guide.html).

### Creating the k8s manifest

Use the [example manifest file](example/s3-test.yaml) that runs the simple Pi job above as a template for your own .yaml file.

Let's highlight some of the things you can control in this file. The first is that the template requires a docker image as a base image to run the job. The `spec.image` specified in the example is a bare-bones Spark 3.0.0 image with Hadoop 2.9.2 and the necessary libraries installed to enable S3 access over Hadoop. You can either use the image as-is and pull in data and dependencies over S3 (as in the example pi.py), or you can use it as a base image and create a new docker image that contains all the required resources that can be accessed directly from python e.g. `rdd = sc.textFile("file:///path/to/file")`.

`spec.mainApplicationFile` points to the Python script that was uploaded in the previous step. Parameters can be passed to the Python script with the `arguments` property.

Care should be taken when allocating resources to the Executors. If Kubernetes isn't able to schedule an executor, the job will get stuck at that point. Take into account that there are other applications also utilising some resources on the cluster at any time. A rule-of-thumb for now is to keep total number of cores to 100 or less and total RAM to 100GB or less.

### Running the job

You will need Sparkctl to effectively run and monitor the job. Take a look [here](Sparkctl.md) to set it up along with kubectl in your environment. Once configured, the process to run a job is quite simple.

First, create the Spark application:

```bash
sparkctl -n spark-jobs create s3-test.yaml
```

After about 5 seconds, you can start monitoring the job by tailing the Driver logs:

```bash
sparkctl -n spark-jobs log s3-test -f
```

Delete it after it has completed:

```bash
sparkctl -n spark-jobs delete s3-test
```

You can also use Sparkctl to list all jobs, get the status of a job, show k8s events for all pods that are part of a job, and even forward the Spark Web UI to a local port (default is port 4040) - although, this will only work while the job is still running.

```bash
sparkctl -n spark-jobs status <SparkApplication name>
sparkctl -n spark-jobs event <SparkApplication name> [-f]
sparkctl -n spark-jobs log <SparkApplication name> [-e <executor ID, e.g., 1>] [-f]
sparkctl -n spark-jobs forward <SparkApplication name> [--local-port <local port>] [--remote-port <remote port>]
```

Also look at the [user guide](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator/tree/master/sparkctl) to see how `sparkctl` can work directly with files on S3 storage.

The Spark History Server ([https://sp-history.lincsproject.ca/](https://sp-history.lincsproject.ca/)) will maintain a copy of the logs so that all jobs can still be viewed in the Spark Web UI after they have completed.

## Understanding Spark on LINCS

Please look at [Operator.md](Operator.md) to see what was involved with the Spark Operator installation and also at [History.md](History.md) for the Spark History Server in the LINCS infrastructure. [This](https://github.com/uprush/kube-spark) repository also contains many resources and examples for running Spark scripts on k8s with an S3 storage backend.

## S3 performance issues

There still needs to be some tuning done to optimise the performance of the S3 storage with Hadoop. Have a look at [this](https://towardsdatascience.com/apache-spark-with-kubernetes-and-fast-s3-access-27e64eb14e0f) article that discusses the issues and solutions.

Some key Spark parameters (In e.g. the SparkApplication .yaml) to tune include the following:

```yaml
spec:
  sparkConf:
    spark.hadoop.fs.s3a.multiobjectdelete.enable: false
    spark.hadoop.fs.s3a.fast.upload: true
    spark.sql.parquet.filterPushdown: true
    spark.sql.parquet.mergeSchema: false
    spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version: 2
    spark.speculation: false
```
