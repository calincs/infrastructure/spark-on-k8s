# Setting up Sparkctl

This document will guide you through the process of getting `sparkctl` to work on the LINCS Spark cluster in Kubernetes. These instruction are for Ubuntu but you can translate them to Windows or other distros.

## Kubectl first

To get access to the LINCS k8s cluster you will need Rancher access. The roles and authorisations defined for your Rancher user will also be applied when using `kubectl` with the kubeconfig from Rancher. You need to tell the cluster administrator that you need to be added to the `research` project so that you can access the `jobs` namespace. Your user will also need a role-binding to the spark-operator role in order to create jobs. The cluster admin can use a modified version of this script to create the binding:

```yaml
# Role for spark-on-k8s-operator to create resources on cluster
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: spark-cluster-cr
  labels:
    rbac.authorization.kubeflow.org/aggregate-to-kubeflow-edit: "true"
rules:
  - apiGroups:
      - sparkoperator.k8s.io
    resources:
      - sparkapplications
    verbs:
      - '*'
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: sparkoperator-rancher_user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: spark-cluster-cr
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: rancher_user_code
  namespace: spark-jobs
```

Let's install kubectl first. Other environments look [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/) for instructions.

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
kubectl version --client
```

Now we need to tell kubectl to connect to the LINCS cluster. Log into [Rancher](https://rancher.lincsproject.ca/) and click on the `Production` cluster. If you can't see it, it means that your user doesn't have the correct access. Make sure you're on the `Cluster` tab and click the `Kubeconfig File` button. Hit the `Copy to Clipboard` link on the bottom of that window.

Create the kubeconfig by running `nano ~/.kube/config` and then hitting *shift-insert* to paste the content and then *ctrl-x + y* to save. Check that you are connecting to the right cluster:

```bash
kubectl config view
```

## Building Sparkctl

You need to clone the repo, install `Go`, compile the sources, and copy the executable.

```bash
git clone https://github.com/GoogleCloudPlatform/spark-on-k8s-operator
cd spark-on-k8s-operator/sparkctl
sudo apt-get install golang
go build -o sparkctl
sudo mv sparkctl /usr/sbin/
```

You should now be able to run `sparkctl` in the context of the production environment. Check the readme out [here](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator/tree/master/sparkctl) if you want to know more.
