# Spark Operator for LINCS

Only the Spark Operator configuration is described here. For the Spark History Server, please look at the readme [here](History.md).

## The Operator and CRDs

The official [Helm chart](https://github.com/helm/charts/tree/master/incubator/sparkoperator) was used to install the operator. The [user guide](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator/blob/master/docs/quick-start-guide.md) explains all the options but only the following was changed from default:

```bash
kubectl create namespace spark-jobs
helm repo add spark-operator https://googlecloudplatform.github.io/spark-on-k8s-operator
helm repo update
helm install spark-operator spark-operator/spark-operator --namespace spark-operator --create-namespace --set sparkJobNamespace=spark-jobs,enableWebhook=true
```

That is all that is required for it to recognise `SparkApplication` manifests and start Spark jobs. We even get Prometheus metrics right out of the box.

### Spark Application Metrics

| Metric | Description |
| ------------- | ------------- |
| `spark_app_count`  | Total number of SparkApplication handled by the Operator.|
| `spark_app_submit_count`  | Total number of SparkApplication spark-submitted by the Operator.|
| `spark_app_success_count` | Total number of SparkApplication which completed successfully.|
| `spark_app_failure_count` | Total number of SparkApplication which failed to complete. |
| `spark_app_running_count` | Total number of SparkApplication which are currently running.|
| `spark_app_success_execution_time_microseconds` | Execution time for applications which succeeded.|
| `spark_app_failure_execution_time_microseconds` | Execution time for applications which failed. |
| `spark_app_start_latency_microseconds` | Start latency of SparkApplication as type of [Prometheus Summary](https://prometheus.io/docs/concepts/metric_types/#summary). |
| `spark_app_start_latency_seconds` | Start latency of SparkApplication as type of [Prometheus Histogram](https://prometheus.io/docs/concepts/metric_types/#histogram). |
| `spark_app_executor_success_count` | Total number of Spark Executors which completed successfully. |
| `spark_app_executor_failure_count` | Total number of Spark Executors which failed. |
| `spark_app_executor_running_count` | Total number of Spark Executors which are currently running. |

### Work Queue Metrics

| Metric | Description |
| ------------- | ------------- |
| `spark_application_controller_depth` | Current depth of workqueue |
| `spark_application_controller_adds` | Total number of adds handled by workqueue |
| `spark_application_controller_latency` | Latency for workqueue |
| `spark_application_controller_work_duration` | How long processing an item from workqueue takes |
| `spark_application_controller_retries` | Total number of retries handled by workqueue |
| `spark_application_controller_unfinished_work_seconds` | Unfinished work in seconds |
| `spark_application_controller_longest_running_processor_microseconds` | Longest running processor in microseconds |

## User Permissions

To give a service account or a user the ability to create and execute jobs, give them the `sparkoperator-cr` role e.g.:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: sparkoperator-testuser
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: sparkoperator-cr
subjects:
- kind: User
  name: u-4dl2m
  namespace: spark-jobs
```

## S3 Access

We have to have a secret in the spark-operator namespace with the S3 credentials for S3 access to work. Run the following to create the secret:

```bash
kubectl -n spark-operator create secret generic aws-secrets --from-literal=aws-access-key=recon-api --from-literal=aws-secret-key=<the password>
```
